<?php
session_start();

if(!isset($_GET['path']))
{
    $path="main";
}
else{
    $path=$_GET['path'];
}
    switch ($path){
        case "main":
        require('controller/controller.php');
        break;

        case "admin":
        require('controller/adminController.php');
        break;

        default :
        require("view/404.php");
    }
?>